// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  env: 'development',
  production: false
};


export let API_URL = 'http://localhost:3000/';


switch (environment.env) {
  case 'development': 
  API_URL = 'http://localhost:3000/';
  break;
}

if (environment.production === true) {}