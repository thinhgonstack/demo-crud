import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class AuthGuard implements CanActivate {

    constructor(private route: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        const keyLogin = localStorage.getItem('isLoggedin') ? JSON.parse(localStorage.getItem('isLoggedin')) : false;
        console.log('keyLogin', keyLogin)
        if (!keyLogin) {
            this.route.navigate(['login'])
            // if we return true user allowed to access that route
            return false;
        }
        // if we return false user is not allowed to access
        return true;
    }
}