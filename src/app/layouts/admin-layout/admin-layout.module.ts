import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContactComponent } from 'app/pages/contact/contact.component';
import { ContactModule } from 'app/pages/contact/contact.module';

@NgModule({
    imports: [
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        NgbModule,
        ReactiveFormsModule,
        ContactModule
    ],
    declarations: [
        DashboardComponent,
        IconsComponent,
        MapsComponent,
        ContactComponent
    ]
})

export class AdminLayoutModule { }
