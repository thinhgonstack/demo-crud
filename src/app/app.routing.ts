import { Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './pages/login/login.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent,
        data: {
            title: 'Login page'
        },
        canActivate: []
    },

    {
        path: '',
        component: AdminLayoutComponent,
        data: {
            title: 'Home page'
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'contact',
                loadChildren: () => import('./pages/contact/contact.module').then(m => m.ContactModule)
            },
            {
                path: 'maps',
                loadChildren: () => import('./pages/maps/maps.module').then(m => m.MapsModule)
            },
            {
                path: 'icons',
                loadChildren: () => import('./pages/icons/icons.module').then(m => m.IconsModule)
            }
        ],
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: 'dashboard'
    },
   
]
