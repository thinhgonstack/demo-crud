export class Contact {
    id: string;
    name: string;
    email: string;
    phone: number;
    gender: string;
    description: string
}