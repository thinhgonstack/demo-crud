import * as moment from 'moment';
import * as uuid from 'uuid';

export class Helper {
    /**
     * ========GET START AND LAST DAY OF ANY MONTH========
     * let startDayOfPeriod = Helper.setDate(2018, 0, 1); // 0: JANUARY
     * let endDayOfPeriod = Helper.setDate(2018, 0 + 1, 0);
     */
    constructor() {
        return;
    }
    static uuid() {
        return uuid.v4();
    }
    static now_timestamp() {
        return moment.now();
    }

    static now() {
        return moment(new Date()).local();
    }

    static utcNow() {
        return moment(new Date()).utc();
    }

    /**
     *
     * @param {*} inputString
     * @param {*} stringFormat
     */
    static stringToTimestamp(inputString, stringFormat = 'DD/MM/YYYY HH:mm') {

        return moment(inputString, stringFormat).local().unix() * 1000;
    }

    /**
     *
     * @param {*} timestamp
     * @param {*} outFormat
     */
    static timestampToString(timestamp, outFormat = 'DD/MM/YYYY HH:mm') {
        return moment(timestamp).local().format(outFormat);
    }
    static stringToTimestampExcel(timestamp, outFormat = 'DD/MM/YYYY') {
        return moment(timestamp).local().format(outFormat);
    }

    /**
     *
     * @param {*} timestamp
     * @returns {years, months, date, hours, minutes, seconds, milliseconds}
     */
    static timestampToObject(timestamp) {
        if (!timestamp || timestamp < 0) {
            return null;
        }
        return moment(timestamp).local().toObject();
    }
    /**
     * milisecond
     */
    static utcTimestampNow(inputTime = null) {
        let now = moment(new Date()).utc();
        if (inputTime) {
            now = moment(inputTime).utc();
        }
        return now.unix() * 1000 + now.millisecond();
    }


    static datetime(inputTime, inputFormat = null, outputFormat = null) {
        if (inputFormat) {
            if (outputFormat) {
                return moment(inputTime, inputFormat).format(outputFormat);
            }
            return moment(inputTime, inputFormat);
        } else {
            if (outputFormat) {
                return moment(inputTime).format(outputFormat);
            }
            return moment(inputTime);
        }
    }

    static localNowString(format = 'HH:mm:ss DD-MM-YYYY') {
        return moment(new Date()).local().format(format);
    }

    static utcToLocal(utcTime, format = 'HH:mm:ss DD-MM-YYYY') {
        return moment(utcTime).local().format(format);
    }

    static localToUtc(localTime, options: any) {
        if (!options || !options.format) {
            options.format = 'HH:mm:ss DD-MM-YYYY';
        }
        return moment(localTime).utc().format(options.format);
    }

    /**
     *
     * @param {*} utcTime
     * @return miliseconds
     */
    static utcToTimestamp(utcTime, options: any) {
        let utcMomentTime = this.utcNow();
        if (utcTime) {
            // if (options && options.inputFormat) {
            //   utcMomentTime = moment().utc(utcTime, options.inputFormat);
            // } else {
            utcMomentTime = moment().utc(utcTime);
            // }
        }
        return utcMomentTime.unix() * 1000 + utcMomentTime.millisecond();
    }

    /**
     * convert local time to utc timestamp
     * @param {*} localTime
     * @param {*} options
     */

    static localToUtcTimestamp(localTime, options: any) {
        let localMomentTime = this.now();
        if (localTime) {
            if (options && options.inputFormat) {
                localMomentTime = moment(localTime, options.inputFormat);
            } else {
                localMomentTime = moment(localTime);
            }
        }
        const result = localMomentTime.utc().unix() * 1000 + localMomentTime.millisecond();
        return result;
    }

    /**
     * milisecond
     */
    static datetimeToTimestamp(inputTime = null) {
        let now = moment(new Date());
        if (inputTime) {
            now = moment(inputTime);
        }
        return now.unix() * 1000 + now.millisecond();
    }

    static datetimeToObject(datetime, isUtc = false) {
        try {
            if (!datetime) {
                datetime = new Date();
            }
            if (moment.isMoment(datetime)) {
                if (isUtc) {
                    return datetime.local().toObject();
                } else {
                    return datetime.toObject();
                }
            } else {
                if (isUtc) {
                    return moment(datetime).local().toObject();
                } else {
                    return moment(datetime).toObject();
                }
            }
        } catch {
            return null;
        }
    }

    static getWeekDayString(idx) {
        const i = parseInt(idx);
        if (i == null) {
            return '';
        }
        switch (i) {
            case 0:
                return 'Chủ nhật';
            case 1:
                return 'Thứ 2';
            case 2:
                return 'Thứ 3';
            case 3:
                return 'Thứ 4';
            case 4:
                return 'Thứ 5';
            case 5:
                return 'Thứ 6';
            case 6:
                return 'Thứ 7';
        }
    }

    static getStartDayTime(inputTime) {
        const t = moment(inputTime);
        t.set('hour', 0);
        t.set('minute', 0);
        t.set('second', 0);
        t.set('millisecond', 0);
        return t;
    }

    static getEndDayTime(inputTime) {
        const t = moment(inputTime);
        t.set('hour', 23);
        t.set('minute', 59);
        t.set('second', 59);
        t.set('millisecond', 999);
        return t;
    }

    static getStartDayOfWeek(currentDay) {
        let first = null;
        if (currentDay.getDay() === 0) {
            first = currentDay.getDate() - 7 + 1;
        } else {
            first = currentDay.getDate() - currentDay.getDay() + 1; // First day is the day of the month - the day of the week
        }
        const firstday = new Date(currentDay.setDate(first)).toUTCString();
        return firstday;
    }

    static getLastDayOfWeek(currentDay) {
        let first = null;
        if (currentDay.getDay() === 0) {
            first = currentDay.getDate() - 7 + 1;
        } else {
            first = currentDay.getDate() - currentDay.getDay() + 1;
        }
        const last = first + 6; // last day is the first day + 6
        const lastday = new Date(currentDay.setDate(last)).toUTCString();
        return lastday;
    }

    static getBeforeDayOfWeek(currentDay) {
        let first = null;
        if (currentDay.getDay() === 0) {
            first = currentDay.getDate() - 7 + 1;
        } else {
            first = currentDay.getDate() - currentDay.getDay() + 1;
        }
        const last = first - 6; // last day is the first day + 6
        const lastday = new Date(currentDay.setDate(last)).toUTCString();
        return lastday;
    }

    static setDate(y = null, m = null, d = null) {
        const today = new Date();
        if (y == null) {
            y = today.getFullYear();
        }
        if (m == null) {
            m = today.getMonth();
        } else {
            m = m - 1;
        }
        if (d == null) {
            d = today.getDate();
        }
        today.setFullYear(y, m, d);
        return today;
    }
    /**
     * @param {*} value
     */
    static paymentPrediction(value) {
        const maxValue = 1000000000;
        const useageList = [0, 1000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000];
        const suggestList = [];
        useageList.forEach(money => {
            let flag = true;
            let i = 0;
            let a = maxValue;
            while (flag && a >= money) {
                i += 1;
                const odd = Math.floor(parseFloat(value) % a);

                const res = value - odd + money;
                if (!suggestList.includes(res) && res >= value) {
                    suggestList.push(res);
                }

                if (Math.floor(parseFloat(value) % a) <= 0) {
                    flag = false;
                }
                a = a / 10;
            }
        })
        return suggestList;
    }


    /**
     *
     * @param {*} type
     * @param {*} base
     * @param {*} uppercase
     */
    static generateHashNumber(base = 16): number {
        base -= 1;
        let charsets = '1234567890';
        const first_character = String(Math.floor(Math.random() * 9) + 1);

        let hash = '';
        for (let i = 0; i < base; i++) {
            hash += String(charsets[Math.floor(Math.random() * charsets.length)]);
        }
        return Number(first_character + hash);
    }


    /**
     *
     * @param {*} type
     * @param {*} base
     * @param {*} uppercase
     */
    static generateHashString(type = 'mix', base = 16, uppercase = false): string {
        let charsets = null;
        let first_character = '';
        if (type === 'number') {
            first_character = String(Math.floor(Math.random() * 9) + 1);
            base -= 1;
            charsets = '1234567890';
        } else if (type == 'string') {
            charsets = 'abcdefghijklmnopqrstuvwxyz';
        } else {
            charsets = 'abcdefghijklmnopqrstuvwxyz1234567890';
        }

        if (uppercase) {
            charsets = charsets.toUpperCase().split('');
        } else {
            charsets = charsets.split('');
        }
        let hash = '';
        for (let i = 0; i < base; i++) {
            hash += String(charsets[Math.floor(Math.random() * charsets.length)]);
        }
        return first_character + hash;
    }


    static validatePhone(number) {
        // const phoneno = /(09|08|07|05|03)+[0-9]{8}/g;
        // const result = inputPhone.match(phoneno);
        // if (result && result === inputPhone) {
        //     return true;
        // } else {
        //     return false;
        // }
        // return /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/.test(number);
        return /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/.test(number);
    }
    /**
     *
     * @param {*} previousItems: itemA
     * @param {*} newItems: itemB
     */

    static formatCurrency(amount: number, toFixed: number = 0, unit: string = 'đ') {
        try {
            if (amount !== null && amount !== undefined) {
                return Number(Number(amount).toFixed(toFixed)).toLocaleString('vi') + unit;
            } else {
                return 0;
            }
        } catch (error) {
            return 0;
        }
    }

    static formatNumber(num: number, toFixed: number = 0) {
        let x = 10 ^ toFixed;
        return Math.round(num * x) / x;
        // return Number(num).toFixed(toFixed);
    }




    static upperCaseFirstLetter(text: string) {
        return text[0].toUpperCase() + text.slice(1);
    }
    static getCalendarConfig(locale: string = 'vi') {
        const CALENDER_CONFIG_VI = {
            firstDayOfWeek: 1,
            dayNames: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            dayNamesShort: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            dayNamesMin: ['T2', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            monthNames: ['Tháng 1', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
                'November', 'December'],
            monthNamesShort: ['T1', 'T2', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Today',
            clear: 'Clear',
        };
        return CALENDER_CONFIG_VI;
        // if (locale = 'vi') {
        //   return CALENDER_CONFIG_VI;
        // } else {
        //   return CALENDER_CONFIG_VI;
        // }
    }
    static removeAccents(value) {
        return value.replace(/[àáâãäåắấạãẵaặả]/g, 'a')
            .replace(/[ẩậ]/g, 'a')
            .replace(/[êệếề]/g, 'e')
            .replace(/[úùụưừựứ]/g, 'u')
            .replace(/[íìịĩ]/g, 'i')
            .replace(/[ôốồộõỗoơờợớỡổ]/g, 'o')
            .replace(/[ýỳỹ]/g, 'y')
            .replace(/đ/g, 'd');
    }
    static similarity(s1: any, s2: any) {
        const sort = str => str.split('').sort((a, b) => a.localeCompare(b)).join('');
        let longer = s1;
        let shorter = s2;
        let res = 0;
        if (s1.length < s2.length) {
            longer = s2;
            shorter = s1;
        }
        var longerLength = longer.length;
        if (longerLength == 0) {
            return 1.0;
        }
        let string1 = this.removeAccents(sort(longer));
        let string2 = this.removeAccents(sort(shorter));
        console.log('longer', string1);
        console.log('shorter', string2);
        res = (longerLength - this.editDistance(string1, string2)) / parseFloat(longerLength);
        console.log('result', res);
        return res;
    }

    static editDistance(s1: string, s2: string) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
        var costs = new Array();
        for (var i = 0; i <= s1.length; i++) {
            var lastValue = i;
            for (var j = 0; j <= s2.length; j++) {
                if (i == 0)
                    costs[j] = j;
                else {
                    if (j > 0) {
                        var newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue),
                                costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0)
                costs[s2.length] = lastValue;
        }
        return costs[s2.length];
    }
    static lockFunctionKey(doc) {
        doc.unbind('keydown').bind('keydown', function (e) {
            switch (e.key) {
                case 'F1':
                    e.preventDefault();
                    break;
                case 'F2':
                    e.preventDefault();
                    break;
                case 'F3':
                    e.preventDefault();
                    break;
                case 'F4':
                    e.preventDefault();
                    break;
                case 'F5':
                    e.preventDefault();
                    break;
                case 'F6':
                    e.preventDefault();
                    break;
                case 'F7':
                    e.preventDefault();
                    break;
                case 'F8':
                    e.preventDefault();
                    break;
                case 'F9':
                    e.preventDefault();
                    break;
                case 'F10':
                    e.preventDefault();
                    break;
                case 'F11':
                    e.preventDefault();
                    break;
            }
        });
    }

    static clone = (value: any) => {
        try {
            return JSON.parse(JSON.stringify(value));
        } catch (e) {
            return value;
        }
    }
    static similar(a, b) {
        let equivalency = 0;
        const minLength = (a.length > b.length) ? b.length : a.length;
        const maxLength = (a.length < b.length) ? b.length : a.length;
        for (let i = 0; i < minLength; i++) {
            if (a[i] == b[i]) {
                equivalency++;
            }
        }
        const weight = equivalency / maxLength;
        return (weight * 100) + '%';
    }

    static convertAmount(amount: number = 0, currency_unit: string = null) {
        const divisor = Math.pow(10, 6);
        if (currency_unit == null) {
            return (amount / divisor).toFixed(2);
        }
        return (amount / divisor).toFixed(2) + + currency_unit;
    }
    static currency(amount: number) {
        if (amount) {
            return amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        } else {
            return 0;
        }
    }
    static checkRoleUser(permission: any): any {
        if (permission) {
            return {
                'read': permission['read'],
                'update': permission['update'],
                'create': permission['create'],
                'delete': permission['delete'],
                'execute': permission['execute']
            }
        }
    }
    static checkOnlyReadRole(permission: any): boolean {
        const per = this.checkRoleUser(permission);
        console.log('per', per)
        if (per['update'] == false && per['delete'] == false && per['create'] == false && per['read'] == true) {
            return true
        } {
            return false;
        }
    }

    static checkPermissionHas(readPer: boolean, updatePer: boolean, createPer: boolean, deletePer: boolean): string {
        if (readPer == false) {
            return 'noPer';
        } else if (updatePer == false && createPer == false && deletePer == false && readPer == true) {
            return 'readonlyPer';
        } else if (updatePer == true && createPer == false && deletePer == false && readPer == true) {
            return 'readUpdatePer';
        } else if (updatePer == false && createPer == true && deletePer == false && readPer == true) {
            return 'readCreatePer';
        } else if (updatePer == false && createPer == false && deletePer == true && readPer == true) {
            return 'readDeletePer';
        } else if (updatePer == true && createPer == true && deletePer == false && readPer == true) {
            return 'readCreateUpdate';
        } else if (updatePer == true && deletePer == true && createPer == false && readPer == true) {
            return 'readDeleteUpdatePer';
        } else if (updatePer == false && deletePer == true && createPer == true && readPer == true) {
            return 'readDeleteCreatePer';
        } else if (updatePer == true && deletePer == true && createPer == true && readPer == true) {
            return 'readDeleteCreateUpdatePer';
        }
    }
    static debounce = (func, wait, immediate) => {
        let timeout;
        function _debounce(...args) {
            const context = this;
            const later = function __debounce() {
                timeout = null;
                if (!immediate) { func.apply(context, args); }
            };
            const callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) { func.apply(context, args); }
        }
        _debounce.stop = () => clearTimeout(timeout);
        return _debounce;
    }

}
