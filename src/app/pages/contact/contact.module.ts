import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { ContactRoutingModule } from './contact-routing.module';
// import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    ContactRoutingModule,
    // BrowserModule
  ]
})
export class ContactModule { }
