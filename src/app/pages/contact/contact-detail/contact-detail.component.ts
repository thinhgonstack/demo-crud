import { Component, Inject, Input, OnInit } from '@angular/core';
import { Contact } from 'app/models/contact';
import { ContactService } from 'app/services/contact.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
    selector: 'app-contact-detail',
    templateUrl: './contact-detail.component.html',
    styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {

    horizontalPosition: MatSnackBarHorizontalPosition = 'end';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    public contactId: string;
    public contact: Contact = new Contact();
    public genders = ['Male', 'Female', 'Orther'];
    contactForm: FormGroup;
    submitted = false;


    constructor(
        private contactService: ContactService,
        private formBuider: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<ContactDetailComponent>,
        private _snackBar: MatSnackBar
    ) { }

    ngOnInit(): void {
        this.contact = this.data;
        this.contactForm = this.formBuider.group({
            name: ['', [Validators.required, Validators.minLength(5)]],
            email: ['', [Validators.required, Validators.email]],
            gender: ['', Validators.required],
            phone: ['', [Validators.required, Validators.minLength(10)]]
        });

    }

    get f() { return this.contactForm.controls; }

    onSubmit() {
        console.log('vao')
        this.submitted = true;

        // stop here if form is invalid
        if (this.contactForm.invalid) {
            console.log('invalid', this.contactForm.invalid)
            return;
        }
        // display form value on success
        this.save();

    }

    save(): void {
        if (this.contact.id) {
            this.update();
        } else {
            this.create();
        }
    }

    update(): void {
        console.log('this.contactId', this.contact.id)
        this.contactService.update(this.contact, this.contact.id).subscribe(
            (res) => {
                this.notification();
                this.closeDialog(res);
            },
            error => {
                console.log(error)
            },
        );
    }

    create(): void {
        this.contact.id += this.contact.id;
        console.log('this.contact', this.contact)
        this.contactService.create(this.contact).subscribe(
            (res) => {
                this.notification();
                this.closeDialog(res);
            },
            error => {
                console.log(error)
            },
        )
    }

    closeDialog(contact: any): void {
        this.dialogRef.close(contact);
    }

    onEvent($event: string): void {
        console.log($event);
        if ($event) {
            this.contact.gender = $event;
        }
    }

    notification() {
        this._snackBar.open('Update success!!', 'View', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
        });
    }

}
