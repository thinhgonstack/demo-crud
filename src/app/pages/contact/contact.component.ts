import { Component, OnDestroy, OnInit } from '@angular/core';
import { Contact } from 'app/models/contact';
import { ContactService } from 'app/services/contact.service';
import { MatDialog } from '@angular/material/dialog';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Observable, Subject, Subscription } from 'rxjs';
import { LoaderService } from 'app/services/loader.service';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html'
})
export class ContactComponent implements OnDestroy, OnInit {

    horizontalPosition: MatSnackBarHorizontalPosition = 'end';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    public contacts: Array<Contact>;
    subscribtion: Subscription;
    id = Date.now();
    observable: Observable<number>


    constructor(
        public spinner: LoaderService,
        public dialog: MatDialog,
        private contactService: ContactService,
        private _snackBar: MatSnackBar,


    ) { }

    ngOnInit(): void {
        this.subscribtion = this.contactService.getPaging()
            .subscribe(
                (res) => {
                    this.contacts = res
                    console.log('this.contacts', this.contacts)
                },
                (error) => { }
            );
        
        // this.observable = Observable.create(observer => {
        //     let value = 0;
        //     const interval = setInterval(() => {
        //         observer.next(value)
        //         value++;
        //     }, 1000) 
        //     return () => clearInterval(interval);
        // })

        // console.log('====>observable', this.observable)

    }

    deleteItem(id: string): void {
        alert('You can sure ?')
        this.contactService.delete(id).subscribe(
            (next) => {
                this.notification()
                this.ngOnInit();
            },
            error => {
                console.log(error)
            }
        )
    }

    openDialog($event: any): void {
        console.log('$event', $event);
        const dialogRef = this.dialog.open(ContactDetailComponent, { data: $event ? $event : new Contact() });
        dialogRef.afterClosed().subscribe(
            (result) => {
                if (result) {
                    if (result.id == $event.id) {
                        this.ngOnInit();
                    } else {
                        console.log('result', result);
                        this.contacts.push(result);
                    }
                }
            }
        );
    }

    notification() {
        this._snackBar.open('Update success!!', 'View', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,

        });
    }

    searchByName($event: any): void {
        console.log('$event', $event)
        this.contactService.searchByName($event).subscribe(
            (res) => {
                if (res) {
                    this.contacts = res;
                }
            }
        )
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscribtion.unsubscribe();
        console.log('unsubscribe', this.subscribtion)

        // After exit page is it will destroy 
        console.log("Component Destroyed " + this.id);

    }
}
