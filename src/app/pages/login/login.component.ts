import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Authen } from 'app/models/authen';
import { AuthService } from 'app/services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    authen: Authen = new Authen();

    loginForm: FormGroup;
    submitted = false;

    constructor(
        private router: Router,
        private authenService: AuthService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        })
    }

    get f() { return this.loginForm.controls }

    onSubmit(): void {
        this.submitted = true;
        if (this.loginForm.invalid) {
            console.log('this.loginForm.invalid', this.loginForm.invalid)
            return;
        }
        this.login();
    }

    login(): void {
        this.authenService.login(this.authen.username, this.authen.password).subscribe(
            (res) => {
                console.log('res', res);
                if (res) {
                    console.log('res', res);
                    alert('Login Successed')
                    this.router.navigateByUrl('dashboard')
                    localStorage.setItem('isLoggedin', 'true');
                    this.submitted = false;
                }
            },
            (error) => {
                console.log('error', error)
                alert('Login failed')
                this.submitted = false;
            }
        )
    }

}
