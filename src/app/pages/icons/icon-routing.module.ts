import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IconsComponent } from './icons.component';


const routes: Routes = [
    {
        path: '',
        redirectTo: 'icons'
    },
    {
        path: '',
        component: IconsComponent
    }
]


@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class IconRoutingModule { }
