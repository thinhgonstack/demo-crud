import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError, filter, finalize, map, tap } from "rxjs/operators";
// import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { LoaderService } from "./loader.service";

@Injectable()
export class AppInterCeptor implements HttpInterceptor {
    constructor(
        public spinner: LoaderService
    ) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.spinner.visible$.next(true);
        return next.handle(req).pipe(
           finalize(
               () => {
                   this.spinner.visible$.next(false);
               }
           )
       )
    }
    
}