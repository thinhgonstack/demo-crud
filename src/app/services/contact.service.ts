import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { interval, Observable, throwError } from 'rxjs';
import { Contact } from 'app/models/contact';
import { catchError, delay, timeout } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ContactService {
    source$ = interval(Math.round(Math.random() * 10000));
    private URL = 'http://localhost:3000/contact/';

    constructor(
        private http: HttpClient
    ) { }

    getPaging(): Observable<Contact[]> {
        return this.http.get<Contact[]>(this.URL).pipe(
            catchError(this.errorMgmt)
        )
    }

    getById(id: string): Observable<Contact> {
        return this.http.get<Contact>(this.URL + id).pipe(catchError(this.errorMgmt));
    }

    create(contact: any): Observable<Contact> {
        return this.http.post<Contact>(this.URL, contact).pipe(catchError(this.errorMgmt));
    }

    update(contact: any, id: string): Observable<Contact> {
        return this.http.put<Contact>(this.URL + id, contact).pipe(catchError(this.errorMgmt));
    }

    delete(id: string): Observable<any> {
        return this.http.delete<any>(this.URL + id).pipe(catchError(this.errorMgmt));
    }

    searchByName(name?: string): Observable<any> {
        console.log('name', name)
        if (name == null || name == undefined || name.length == 0) {
            return this.http.get<any>(this.URL);
        }
        return this.http.get<any>(this.URL + `?name=${name}`).pipe(catchError(this.errorMgmt));
    }


    errorMgmt(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error cod: ${error.status}\nMessage: ${error.message}`
        }
        if (error.status == 520) {
            return [];
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }

}
