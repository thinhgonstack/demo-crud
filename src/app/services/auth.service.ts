import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    isLoggedin = false;
    data: any = {
        username: 'admin', 
        password: '123456'
    }

    constructor() { }

    checkLogin() {
        this.isLoggedin = true;
    }

    logout() {
        this.isLoggedin = false;
    }

    login(username?: string, password?: string): Observable<any> {
        console.log(`user: ${username}, password: ${password}`)
        const isAdmin = {
            username: username,
            password: password
        }
        if (isAdmin.username == this.data.username && isAdmin.password == this.data.password) {
            this.isLoggedin = true;
            localStorage.setItem('isLoggedin', JSON.stringify(this.isLoggedin));
            return of(isAdmin);
        }
        return of();
        
    }
}
